export default {
  widgets: [
    // {
    //   name: 'Darkhorse',
    //   options: {
    //     templateRepoId: 'sanity-io/sanity-template-nextjs-landing-pages'
    //   }
    // },
    { name: 'structure-menu' },
    // {
    //   name: 'project-info',
    //   options: {
    //     __experimental_before: [
    //       {
    //         name: 'netlify',
    //         options: {
    //           description:
    //             'NOTE: Because these sites are static builds, they need to be re-deployed to see the changes when documents are published.',
    //           sites: [
    //             {
    //               buildHookId: '603f537b44fb8edf7428ba23',
    //               title: 'Sanity Studio',
    //               name: 'sanity-nextjs-landing-pages-sample-studio',
    //               apiId: '10f5b9d6-6d76-4c70-b181-86f8c876a744'
    //             },
    //             {
    //               buildHookId: '603f537b19abb4059ddb27fe',
    //               title: 'Landing pages Website',
    //               name: 'sanity-nextjs-landing-pages-sample',
    //               apiId: 'dc9b1f65-d92f-48dc-96e9-d9af9cc417b9'
    //             }
    //           ]
    //         }
    //       }
    //     ],
    //     data: [
    //       {
    //         title: 'GitHub repo',
    //         value: 'https://github.com/dev-studiojs/sanity-nextjs-landing-pages-sample',
    //         category: 'Code'
    //       },
    //       {title: 'Frontend', value: 'https://sanity-nextjs-landing-pages-sample.netlify.app', category: 'apps'}
    //     ]
    //   }
    // },
    { name: 'project-users', layout: { height: 'auto' } },
    {
      name: 'document-list',
      options: {
        title: 'Recently edited',
        order: '_updatedAt desc',
        limit: 10,
        types: ['page'],
      },
      layout: { width: 'medium' },
    },
  ],
}
