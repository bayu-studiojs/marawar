import S from '@sanity/base/structure-builder'

const hiddenDocTypes = (listItem) =>
  ![
    'home',
    'page',
    'route',
    'site-config',
    'client',
    'instagramFeed',
    'bottomCta',
    'author',
    'capability',
    'ourPeople',
    'contact',
    'globalSettings',
    'footer',
    'mainNavigation',
    'projectCategory',
    'projectSubCategory',
    'aboutUs',
    'ourImpact',
    'privacyPolicy',
    'newsCategory',
    'mConstruction',
  ].includes(listItem.getId())

export default [
  ...S.defaultInitialValueTemplateItems().filter(hiddenDocTypes)
]