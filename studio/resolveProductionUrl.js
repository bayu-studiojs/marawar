export default function resolveProductionUrl(document) {
  const slug = document.slug?.current
  if (!slug) {
    return undefined
  }

  if (document._type === 'news') {
    return `https://marawar-staging.netlify.app/news/${document.slug.current}/?preview=true`
  }
  return undefined
}
