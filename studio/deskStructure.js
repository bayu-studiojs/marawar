// eslint-disable-next-line no-unused-vars
import React from 'react'
import S from '@sanity/desk-tool/structure-builder'
import client from 'part:@sanity/base/client'
import { createSuperPane } from 'sanity-super-pane';

import { IoSettingsOutline, IoFolderOutline, IoHomeOutline, IoHeartOutline, IoLinkOutline, IoDocumentOutline, IoPeopleOutline } from "react-icons/io5";

// const url = 'https://marawar-staging.netlify.app/'
// const url = 'http://localhost:3000/news/'

// We filter document types defined in structure to prevent
// them from being listed twice
const hiddenDocTypes = (listItem) =>
  ![
    'home',
    'page',
    'route',
    'site-config',
    'client',
    'testimonial',
    'instagramFeed',
    'bottomCta',
    'service',
    'team',
    'news',
    'author',
    'capability',
    'ourPeople',
    'contact',
    'project',
    'globalSettings',
    'footer',
    'mainNavigation',
    'projectCategory',
    'projectSubCategory',
    'aboutUs',
    'ourImpact',
    'awards',
    'privacyPolicy',
    'newsCategory',
    'mConstruction',
  ].includes(listItem.getId())

export default () =>
  S.list()
    .title('Site')
    .items([
      S.listItem()
        .title('Home')
        .icon(IoHomeOutline)
        .child(
          S.editor()
            .id('home')
            .schemaType('home')
            .documentId('home')
      ),
      S.listItem()
        .title('About Us')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('aboutUs')
            .schemaType('aboutUs')
            .documentId('aboutUs')
      ),
      S.listItem()
        .title('Our Impact')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('ourImpact')
            .schemaType('ourImpact')
            .documentId('ourImpact')
      ),
      S.listItem()
        .title('Capabilities')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('capability')
            .schemaType('capability')
            .documentId('capability')
      ),
      S.listItem()
        .title('Services')
        .icon(IoFolderOutline)
        .child(S.documentTypeList('service')),
      S.listItem()
        .title('Our People')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('ourPeople')
            .schemaType('ourPeople')
            .documentId('ourPeople')
        ),
      S.listItem()
        .title('Our Team')
        .icon(IoPeopleOutline)
        .schemaType('team')
        .child(S.documentTypeList('team').title('Our Team')),
      S.listItem()
        .title('News')
        .icon(IoFolderOutline)
        .schemaType('news')
        .child(
          S.list()
            .title('News')
            .items([
              S.listItem()
                .title('Featured')
                .icon(IoFolderOutline)
                .schemaType('news')
                .child(
                  S.documentList()
                    .title('Featured')
                    .filter('_type == "news" && featured == true')
                    .menuItems([
                      S.orderingMenuItem(
                        {
                          title: 'Published Date',
                          by: [
                            { field: 'publishDate', direction: 'desc' },
                          ]
                        }
                      ),
                        S.orderingMenuItem(
                        {
                          title: 'A-Z',
                          by: [
                            { field: 'title', direction: 'asc' },
                          ]
                        },
                      ),
                        S.orderingMenuItem(
                        {
                          title: 'Last Edited',
                          by: [
                            { field: '_updatedAt', direction: 'desc' },
                          ]
                        }
                      )
                    ])
                    .defaultOrdering([{field: 'publishDate', direction: 'asc'}])
                ),
              S.listItem()
                .title('All Posts')
                .icon(IoFolderOutline)
                .schemaType('news')
                .child(
                  S.documentList().title('All Posts')
                  .filter('_type == "news"')
                  .menuItems([
                    S.orderingMenuItem(
                      {
                        title: 'Published Date',
                        by: [
                          { field: 'publishDate', direction: 'desc' },
                        ]
                      }
                    ),
                      S.orderingMenuItem(
                      {
                        title: 'A-Z',
                        by: [
                          { field: 'title', direction: 'asc' },
                        ]
                      },
                    ),
                      S.orderingMenuItem(
                      {
                        title: 'Last Edited',
                        by: [
                          { field: '_updatedAt', direction: 'desc' },
                        ]
                      }
                    )
                  ])
                  .defaultOrdering([{field: 'publishDate', direction: 'asc'}])
                ),
                S.listItem()
                .title('News Categories')
                .child(
                  S.documentTypeList('newsCategory').title('News Categories')
                ),
            ])
        ),
      // S.listItem().title('News').child(createSuperPane('news', S, customColumns)),
      S.listItem()
        .title('Awards')
        .icon(IoFolderOutline)
        .schemaType('awards')
        .child(S.documentTypeList('awards').title('Awards')),
      S.listItem()
        .title('Projects')
        .icon(IoFolderOutline)
        .schemaType('project')
        .child(
          S.list()
            .title('Projects')
            .items([
              S.listItem()
                .title('Featured')
                .icon(IoFolderOutline)
                .schemaType('project')
                .child(
                  S.documentList()
                    .title('Featured')
                    .filter('_type == "project" && featured == true')
                    .menuItems([
                      S.orderingMenuItem(
                        {
                          title: 'Published Date',
                          by: [
                            { field: 'publishDate', direction: 'desc' },
                          ]
                        }
                      ),
                        S.orderingMenuItem(
                        {
                          title: 'A-Z',
                          by: [
                            { field: 'title', direction: 'asc' },
                          ]
                        },
                      ),
                        S.orderingMenuItem(
                        {
                          title: 'Last Edited',
                          by: [
                            { field: '_updatedAt', direction: 'desc' },
                          ]
                        }
                      )
                    ])
                    .defaultOrdering([{field: 'publishDate', direction: 'asc'}])
                ),
              S.listItem()
                .title('All Projects')
                .icon(IoFolderOutline)
                .schemaType('project')
                .child(
                  S.documentList().title('All Projects')
                  .filter('_type == "project"')
                  .menuItems([
                    S.orderingMenuItem(
                      {
                        title: 'Published Date',
                        by: [
                          { field: 'publishDate', direction: 'desc' },
                        ]
                      }
                    ),
                      S.orderingMenuItem(
                      {
                        title: 'A-Z',
                        by: [
                          { field: 'title', direction: 'asc' },
                        ]
                      },
                    ),
                      S.orderingMenuItem(
                      {
                        title: 'Last Edited',
                        by: [
                          { field: '_updatedAt', direction: 'desc' },
                        ]
                      }
                    )
                  ])
                  .defaultOrdering([{field: 'publishDate', direction: 'asc'}])
                ),
                S.listItem()
                .title('Project Categories')
                .child(
                  S.documentTypeList('projectCategory').title('Project Categories')
                ),
            ])
          // S.documentList()
          // .title('Projects')
          // .filter('_type == "project"')
          // .menuItems([
          //   S.orderingMenuItem(
          //     {
          //       title: 'Published Date',
          //       by: [
          //         { field: 'publishDate', direction: 'desc' },
          //       ]
          //     }
          //   ),
          //     S.orderingMenuItem(
          //     {
          //       title: 'A-Z',
          //       by: [
          //         { field: 'title', direction: 'asc' },
          //       ]
          //     },
          //   ),
          //     S.orderingMenuItem(
          //     {
          //       title: 'Last Edited',
          //       by: [
          //         { field: '_updatedAt', direction: 'desc' },
          //       ]
          //     }
          //   )
          // ])
          // .defaultOrdering([{field: 'publishDate', direction: 'asc'}])

        ),
      // S.listItem().title('Project').child(createSuperPane('project', S)),
      
      S.listItem()
        .title('Testimonial')
        .icon(IoHeartOutline)
        .child(S.documentTypeList('testimonial').title('Testimonial')),
      S.listItem()
        .title('Contact')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('contact')
            .schemaType('contact')
            .documentId('contact')
      ),
      S.listItem()
        .title('Privacy Policy')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('privacyPolicy')
            .schemaType('privacyPolicy')
            .documentId('privacyPolicy')
      ),
      S.divider(),
      S.listItem()
        .title('M/Construction')
        .icon(IoDocumentOutline)
        .child(
          S.editor()
            .id('aboutUs')
            .schemaType('mConstruction')
            .documentId('mConstruction')
      ),
      S.divider(),
      S.listItem()
        .title('Routes')
        .icon(IoLinkOutline)
        .schemaType('route')
        .child(S.documentTypeList('route').title('Routes')),
      S.listItem()
        .title('Settings')
        .icon(IoSettingsOutline)
        .child(
          S.list()
            // Sets a title for our new list
            .title('Site Settings')
            // Add items to the array
            // Each will pull one of our new singletons
            .items([
              S.listItem()
                .title('Global Settings')
                .child(
                  S.editor()
                  .id('globalSettings')
                  .schemaType('globalSettings')
                  .documentId('globalSettings')
                  
                ),
              S.listItem()
                .title('Main Navigation')
                .child(
                  S.editor()
                    .schemaType('mainNavigation')
                    .documentId('mainNavigation')
                ),
              S.listItem()
                .title('Footer')
                .child(
                  S.editor()
                    .schemaType('footer')
                    .documentId('footer')
                )
            ])
        ),
      ...S.documentTypeListItems().filter(hiddenDocTypes),
      // S.listItem()
      //   .title('Post')
      //   .icon(MdMenu)
      //   .child(
      //     S.editor()
      //       .id('post')
      //       .schemaType('post')
      //       .documentId('post')
      //       .title('Post')
      //       // Custom view
      //       .views([
      //         S.view.form().icon(MdEdit),
      //         S.view
      //           .component(WebPreview)
      //           .title('Web Preview')
      //           .icon(MdVisibility),
      //       ])
      //   ),
      // ...S.documentTypeListItems().filter(hiddenDocTypes),
    ])
