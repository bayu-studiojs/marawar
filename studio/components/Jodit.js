import React from 'react'
import PropTypes from 'prop-types'
import JoditEditor from 'jodit-react'
import { withDocument } from 'part:@sanity/form-builder'
import FormField from 'part:@sanity/components/formfields/default'
import PatchEvent, { set, unset } from 'part:@sanity/form-builder/patch-event'

const createPatchFrom = (value) =>
  PatchEvent.from(value === '' ? unset() : set(value))

const config = {
  readonly: false,
  height: 400,
}

class Jodit extends React.Component {
  // 5. Declare shape of React properties
  static propTypes = {
    type: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
    }).isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
  }

  editor = React.createRef()

  // Called by the Sanity form-builder when this input should receive focus
  focus = () => {
    this.editor.current.focus()
  }

  // handleUpdate = (event) => {
  //   const editorContent = event.target.innerHTML
  //   setContent(editorContent)
  // }

  handleChange = (value) => {
    const { onChange } = this.props
    onChange(createPatchFrom(value))
  }

  render = () => {
    const { type, value } = this.props

    return (
      <FormField label={type.title} description={type.description}>
        <JoditEditor
          ref={this.editor}
          value={value}
          config={config}
          // onBlur={this.handleUpdate}
          onChange={this.handleChange}
        />
      </FormField>
    )
  }
}

export default withDocument(Jodit)
