/* eslint-disable no-unused-vars */
import React from 'react'

const InternalLinkRender = ({ children }) => <span>{children} 🔗</span>

export default {
  title: 'Internal link to another document',
  name: 'internalLink',
  type: 'reference',
  description: 'Locate a document you want to link to',
  to: [{ type: 'route' }, {type: 'news'}, {type: 'project'}, { type: 'projectCategory'}, { type: 'projectSubCategory'}, {type: 'service'}],
  blockEditor: {
    icon: () => '🔗',
    render: InternalLinkRender,
  },
}
