export default {
  type: 'object',
  name: 'titleAndText',
  title: 'Title And Text',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title',
    },
    {
      name: 'content',
      type: 'simplePortableText',
      title: 'Content',
    },
  ],
  preview: {
    select: {
      title: 'title',
      blocks: 'content',
    },
    prepare: ({title, blocks}) => {
      const block = (blocks || []).find(block => block._type === 'block')
      return {
        title: title,
        subtitle: block.children
            .filter(child => child._type === 'span')
            .map(span => span.text)
            .join(''),
      }
    },
  },
}
