export default {
  type: 'object',
  name: 'textAndImageRight',
  title: 'Text and Image Right',
  fields: [
    {
      name: 'content',
      type: 'blockContent',
      title: 'Content',
    },
    {
      name: 'image',
      type: 'image',
      title: 'Image',
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'content',
      media: 'image',
    },
  },
}
