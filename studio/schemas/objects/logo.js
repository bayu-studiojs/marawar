export default {
  type: 'object',
  name: 'logo',
  title: 'Logo',
  fields: [
    {
      name: 'name',
      type: 'string',
      title: 'Name',
    },
    {
      name: 'logo',
      type: 'image',
      title: 'Logo Image',
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
        },
      ],
    },
    {
      name: 'url',
      type: 'url',
      title: 'Url',
    },
  ],
  preview: {
    select: {
      title: 'name',
      media: 'logo',
    },
    prepare({ title, media }) {
      return {
        title,
        subtitle: 'Logo section',
        media,
      }
    },
  },
}
