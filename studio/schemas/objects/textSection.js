export default {
  type: 'object',
  name: 'textSection',
  title: 'Introduction',
  fields: [
    {
      name: 'content',
      type: 'blockContent',
      title: 'Content',
    },
  ],
  preview: {
    select: {
      title: 'content',
    },
    // prepare: (selection) => {
    //   // const { title } = selection
    //   return {
    //     title: 'Introduction',
    //   }
    // },
  },
}
