export default {
  name: 'menuCustom',
  title: 'Menu custom',
  type: 'object',
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      description: `don't start with /, eg: page-name`,
      options: {
        source: (doc, options) => {
          console.log(doc, options)
          const parent = doc.children.find(item => item._key === options.parentPath[1]._key);
          return parent.name;
        },
      },
      validation: (Rule) => Rule.required(),
    },
  ],
  preview: {
    select: {
      slug: 'slug.current',
      pageTitle: 'title',
    },
    prepare({ slug, pageTitle }) {
      return {
        subtitle: slug === '/' ? '/' : `/${slug}`,
        title: `${pageTitle}`,
      }
    },
  },
}