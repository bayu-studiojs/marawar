export default {
  title: 'Menu Item',
  name: 'menuItem',
  type: 'object',
  fields: [
    {
      name: 'menuText',
      title: 'Menu Text',
      type: 'string',
    },
    {
      title: 'Content Item',
      description: 'Reference existing content to be used in the navigation structure.',
      name: 'project',
      type: 'reference',
      to: [{ type: 'project' }, {type: 'news'}, {type: 'service'}],
    },
  ],
  preview: {
    select: {
      title: 'project.title',
      menuText: 'menuText',
      pageSlug: 'project.slug',
    },
    prepare(selection) {
      const { pageSlug, title, menuText } = selection;
      return {
        ...selection,
        title: menuText ? menuText : title,
        subtitle: `slug: ${pageSlug.current}`
      };
    },
  },
};