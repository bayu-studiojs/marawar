export default {
  type: 'object',
  name: 'seo',
  title: 'SEO & Metadata',
  options: {
    collapsible: true,
    collapse: false,
  },
  fields: [
    // {
    //   name: 'description',
    //   type: 'text',
    //   title: 'Meta Description',
    //   description:
    //     'Modify your meta description by editing it right here. We recommend descriptions between 50–160 characters.',
    //   validation: (Rule) => [
    //     Rule.required()
    //       .min(50)
    //       .error('A description of min. 50 characters is required'),
    //     Rule.max(160).warning('Maximum 160 characters'),
    //   ],
    // },
    {
      title: 'Page Title',
      name: 'pageTitle',
      type: 'string',
      initialValue: '',
      description:
        'By default we use (Page Name | Marawar), Recommended: 35-65 characters',
    },
    // {
    //   title: 'Default Meta Title',
    //   name: 'metaTitle',
    //   type: 'string',
    //   description: 'Title used for search engines and browsers.',
    //   validation: (Rule) =>
    //     Rule.max(50).warning(
    //       'Longer titles may be truncated by search engines'
    //     ),
    // },
    {
      title: 'Meta Description',
      name: 'metaDesc',
      type: 'text',
      rows: 3,
      description:
        'We recommend meta descriptions for search engines between 70–320 characters.',
      validation: (Rule) =>
        Rule.max(320).warning(
          'Longer descriptions may be truncated by search engines'
        ),
      initialValue: '',
    },
    {
      title: 'Social Title',
      name: 'shareTitle',
      type: 'string',
      description:
        'The title of your page as you would like for it to appear when displayed on Facebook. By default we use (Page Name | Dark Horse Agency), Recommended: 35-65 characters',
      validation: (Rule) =>
        Rule.max(65).warning('Longer titles may be truncated by social sites'),
      initialValue: '',
    },
    {
      title: 'Social Description',
      name: 'shareDesc',
      type: 'text',
      rows: 3,
      description:
        'By default, we use the Meta Description, Recommended: 70-320 characters',
      validation: (Rule) =>
        Rule.max(320).warning(
          'Longer descriptions may be truncated by social sites'
        ),
      initialValue: '',
    },
    {
      title: 'Social Graphic',
      name: 'shareGraphic',
      type: 'image',
      description:
        'Set the image you would like displayed when this page is shared on social media. ',
    },
  ],
}
