import React from 'react';

const smallBlockquote = () => (
  <span style={{color: 'rgba(56, 93, 174, 1)'}}>H</span>
);
const smallBlockquoteRender = props => (
  <span style={{ color: 'rgba(56, 93, 174, 1)' }}>{props.children}</span>
);

export default {
  title: 'Simple Portable Text',
  name: 'simplePortableText',
  type: 'array',
  of: [
    {
      title: 'Block',
      type: 'block',
      styles: [
        { title: 'H2', value: 'h2' },
        { title: 'H3', value: 'h3' },
        { title: 'H4', value: 'h4' },
        { title: 'Quote', value: 'blockquote' },
        { 
          title: 'Small Quote', 
          value: 'smallBlockquote', 
          blockEditor: 
          {
            icon: smallBlockquote,
            render: smallBlockquoteRender
          } 
        },
      ],
      lists: [
        { title: 'Bullet', value: 'bullet' },
        { title: 'Number', value: 'number' },
      ],
      marks: {
        decorators: [
          { title: 'Strong', value: 'strong' },
          { title: 'Emphasis', value: 'em' },
          { title: 'Code', value: 'code' },
        ],
        // annotations: [{ type: 'link' }, { type: 'internalLink' }],
      },
    },
  ],
}
