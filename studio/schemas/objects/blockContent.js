/**
 * This is the schema definition for the rich text fields used for
 * for this blog studio. When you import it in schemas.js it can be
 * reused in other parts of the studio with:
 *  {
 *    name: 'someName',
 *    title: 'Some title',
 *    type: 'blockContent'
 *  }
 */
import React from 'react'

const smallBlockquote = () => (
  <span style={{color: 'rgba(56, 93, 174, 1)'}}>H</span>
)
const smallBlockquoteRender = props => (
  <span style={{ color: 'rgba(56, 93, 174, 1)' }}>{props.children}</span>
)

export default {
  title: 'Block Content',
  name: 'blockContent',
  type: 'array',
  of: [
    {
      title: 'Block',
      type: 'block',
      // Styles let you set what your user can mark up blocks with. These
      // correspond with HTML tags, but you can set any title or value
      // you want and decide how you want to deal with it where you want to
      // use your content.
      styles: [
        { title: 'Normal', value: 'normal' },
        { title: 'H2', value: 'h2' },
        { title: 'H3', value: 'h3' },
        { title: 'H4', value: 'h4' },
        { title: 'Quote', value: 'blockquote' },
        { 
          title: 'Small Quote', 
          value: 'smallBlockquote', 
          blockEditor: 
          {
            icon: smallBlockquote,
            render: smallBlockquoteRender
          } 
        },
      ],
      lists: [
        { title: 'Bullet', value: 'bullet' },
        { title: 'Number', value: 'number' },
      ],
      // Marks let you mark up inline text in the block editor.
      marks: {
        // Decorators usually describe a single property – e.g. a typographic
        // preference or highlighting by editors.
        decorators: [
          { title: 'Strong', value: 'strong' },
          { title: 'Emphasis', value: 'em' },
        ],
        // Annotations can be any object structure – e.g. a link or a footnote.
        annotations: [
          {
            title: 'URL',
            name: 'link',
            type: 'object',
            fields: [
              {
                title: 'URL',
                name: 'href',
                type: 'url',
              },
            ],
          },
        ],
      },
    },
    // You can add additional types here. Note that you can't use
    // primitive types such as 'string' and 'number' in the same array
    // as a block type.
    {
      title: 'Full width image with caption',
      name: 'imageBlock',
      type: 'imageBlock',
    },
    // {
    //   title: 'Full width image',
    //   name: 'fullWidthImage',
    //   type: 'image',
    //   options: { hotspot: true },
    //   fields: [
    //     {
    //       type: 'string',
    //       name: 'alt',
    //       title: 'Alternative text',
    //       description: `Some of your visitors cannot see images, 
    //         be they blind, color-blind, low-sighted; 
    //         alternative text is of great help for those 
    //         people that can rely on it to have a good idea of 
    //         what\'s on your page.`,
    //       options: {
    //         isHighlighted: true,
    //       },
    //     },
    //     {
    //       title: 'Caption',
    //       type: 'text',
    //       name: 'caption',
    //       description: 'This caption will appears below the image',
    //       rows: 3,
    //       options: {
    //         isHighlighted: true,
    //       },
    //     },
    //   ],
    // },
    // { type: 'figure' },
    // { type: 'embedHTML' },
    // {
    //   title: 'Video',
    //   name: 'videoEmbed',
    //   type: 'videoEmbed',
    // },
    // {
    //   title: 'File Upload',
    //   name: 'fileUpload',
    //   type: 'fileUpload',
    // },
  ],
}
