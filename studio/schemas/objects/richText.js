import Jodit from '../../components/Jodit'

export default {
  name: 'richText',
  title: 'Rich Text Editor',
  type: 'object',
  fields: [
    {
      title: 'Content',
      name: 'richblock',
      type: 'string',
      inputComponent: Jodit,
    },
  ],
}
