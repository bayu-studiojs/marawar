export default {
  name: 'urlAndImage',
  type: 'object',
  title: 'Url and Image',
  fields: [
    {
      name: 'url',
      type: 'url'
    },
    {
      name: 'image',
      type: 'image',
      title: 'Image',
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'url',
      image: 'image',
    },
    prepare(selection) {
      const { title , image } = selection;

      return {
        title: `${title} `,
        media: image,
      };
    },
  },
};