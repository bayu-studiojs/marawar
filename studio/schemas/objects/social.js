// social.js

const services = ['twitter', 'instagram', 'linkedIn', 'facebook', 'youtube']

export default {
  name: 'social',
  type: 'object',
  title: 'Social Media Links',
  /**
   * Loop through the array of service keys, and return the field configuration.
   * The title will be generated from `name`
   *  */
  fields: services.map((name) => ({
    name,
    type: 'url',
    description: `The full URL to the ${name} profile`,
  })),
}
