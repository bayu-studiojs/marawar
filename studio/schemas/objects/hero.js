export default {
  type: 'object',
  name: 'hero',
  title: 'Hero',
  fields: [
    {
      name: 'heading',
      type: 'string',
      title: 'Heading',
      initialValue: '',
      validation: Rule => Rule.required()
    },
    
    {
      title: "Caption",
      name: "caption",
      type: "text",
      initialValue: '',
      rows: 3,
      validation: Rule => Rule.required()
    },
    {
      name: 'mainImage',
      type: 'image',
      title: 'Hero Image',
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
        },
      ],
      validation: Rule => Rule.required()
    },
  ],
  preview: {
    select: {
      title: 'heading',
      media: 'mainImage',
    },
    prepare({ title, media }) {
      return {
        title,
        subtitle: 'Hero section',
        media,
      }
    },
  },
}
