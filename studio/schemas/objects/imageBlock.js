export default {
  type: 'object',
  name: 'imageBlock',
  title: 'Fullwidth image with caption',
  fields: [
    {
      name: 'title',
      title: 'Caption',
      type: 'string',
    },
    {
      name: 'author',
      title: 'Image',
      type: 'image',
      fields: [
        {
          title: 'Caption',
          name: 'caption',
          type: 'string',
        },
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
          description: 'Important for SEO and accessiblity.',
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'title',
      media: 'author',
    },
    prepare({title, media }) {
      return {
        title,
        media,
      }
    },
  },
}
