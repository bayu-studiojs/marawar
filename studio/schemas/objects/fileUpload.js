export default {
  name: 'fileUpload',
  title: 'File Upload',
  type: 'object',
  fields: [
    {
      title: 'File Upload',
      name: 'fileUpload',
      type: 'file',
      description: 'PDF, DOC, XLS',
    },
    {
      name: 'description',
      type: 'string',
      title: 'Description',
      initialValue: '',
    },
  ],
}