// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type'

// We import object and document schemas
import capabilities from './documents/capabilities'
import news from './documents/news'
import awards from './documents/awards'
import route from './documents/route'
import home from './documents/home'
import testimonial from './documents/testimonial'
import team from './documents/team'
import service from './documents/service'
import project from './documents/project'
import ourPeople from './documents/ourPeople'
import contact from './documents/contact'
import privacyPolicy from './documents/privacyPolicy'
import aboutUs from './documents/aboutUs'
import mConstruction from './documents/mConstruction'
import ourImpact from './documents/ourImpact'
// site settings
import footer from './documents/siteSettings/footer'
import globalSettings from './documents/siteSettings/globalSettings'
import mainNavigation from './documents/siteSettings/mainNavigation'
// categories
import projectCategory from './documents/projectCategory'
import newsCategory from './documents/newsCategory'
import projectSubCategory from './documents/projectSubCategory'

// objects types
import blockContent from './objects/blockContent'
import cta from './objects/cta'
import hero from './objects/hero'
// import form from './objects/form'
import textSection from './objects/textSection'
import imageSection from './objects/imageSection'
import simplePortableText from './objects/simplePortableText'
import figure from './objects/figure'
import internalLink from './objects/internalLink'
import link from './objects/link'
import embedHTML from './objects/embedHTML'
import social from './objects/social'
import logo from './objects/logo'
import seo from './objects/seo'
import richText from './objects/richText'
import dayAndTime from './objects/dayAndTime'
import textAndImageRight from './objects/textAndImageRight'
import videoEmbed from './objects/videoEmbed'
import gallery from './objects/gallery'
import titleAndText from './objects/titleAndText'
import fileUpload from './objects/fileUpload'
import urlAndImage from './objects/urlAndImage'
import menuBranch from './objects/menuBranch'
import menuItem from './objects/menuItem'
import menuCustom from './objects/menuCustom'
import imageBlock from './objects/imageBlock'

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    // The following are document types which will appear
    // in the studio.
    route,
    home,
    news,
    awards,
    capabilities,
    testimonial,
    team,
    service,
    project,
    ourPeople,
    contact,
    privacyPolicy,
    aboutUs,
    mConstruction,
    ourImpact,
    footer,
    globalSettings,
    mainNavigation,
    projectCategory,
    newsCategory,
    projectSubCategory,
    // When added to this list, object types can be used as
    // { type: 'typename' } in other document schemas
    blockContent,
    cta,
    hero,
    // form,
    textSection,
    imageSection,
    simplePortableText,
    social,
    logo,
    figure,
    link,
    internalLink,
    seo,
    richText,
    dayAndTime,
    textAndImageRight,
    embedHTML,
    videoEmbed,
    gallery,
    titleAndText,
    fileUpload,
    menuBranch,
    menuItem,
    menuCustom,
    imageBlock,
    urlAndImage,
  ]),
})
