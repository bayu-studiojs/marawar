// import bcp47 from 'bcp47'
// import IconPicker from '../objects/iconPicker'

export default {
  name: 'client',
  type: 'document',
  title: 'Client',
  // https://www.sanity.io/docs/experimental/ui-affordances-for-actions
  __experimental_actions: ['create', 'update', 'publish'],
  fields: [
    {
      name: 'clientlist',
      type: 'array',
      title: 'Client',
      of: [{ type: 'logo' }],
    },
  ],
  preview: {
    prepare: () => {
      return {
        title: 'Client',
      }
    },
  },
}
