export default {
  name: 'testimonial',
  type: 'document',
  title: 'Testimonial',
  // https://www.sanity.io/docs/experimental/ui-affordances-for-actions
  // __experimental_actions: ['create', 'update', 'publish'],
  fields: [
    {
      title: "Persons Name",
      name: "personsName",
      type: "string"
    },
    {
      title: "Position",
      name: "position",
      type: "string"
    },
    {
      title: "MainImage",
      name: "mainImage",
      type: "image"
    },
    {
      title: "Body",
      name: "body",
      type: 'array', 
      of: [{type: 'block'}]
    }
  ],
  preview: {
    select: {
      title: 'personsName',
      subtitle: 'position'
    }
  },
}
