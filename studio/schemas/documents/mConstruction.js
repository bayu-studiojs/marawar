import Tabs from "sanity-plugin-tabs"
export default {
  name: 'mConstruction',
  title: 'M/Construction',
  type: 'document',
  fields: [
    {
      name: "content",
      type: "object",
      inputComponent: Tabs,
      fieldsets: [
        { name: "hero", title: "Hero", options: { sortOrder: 10 } },
        { name: "secondPanel", title: "Second Panel", options: { sortOrder: 20 } },
        { name: "thirdPanel", title: "Third Panel", options: { sortOrder: 30 } },
        { name: "fourthPanel", title: "Fourth Panel", options: { sortOrder: 40 } },
        { name: "fifthPanel", title: "Fifth Panel", options: { sortOrder: 50 } },
      ],
      options: {
        // setting layout to object will group the tab content in an object fieldset border.
        // ... Useful for when your tab is in between other fields inside a document.
        layout: "object"
      },
      fields: [
        {
          name: 'hero',
          type: 'hero',
          fieldset: 'hero',
        },
        {
          title: "Left Content",
          name: "secondLeftContent",
          type: "simplePortableText",
          fieldset: "secondPanel",
        },
        {
          title: "Block Quote",
          name: "blockQuote",
          type: 'textSection',
          fieldset: "secondPanel",
        },
        {
          title: "Third Panel",
          name: "thirdPanelSection",
          fieldset: "thirdPanel",
          type: "object",
          fields: [
            {
              title: "Left Heading",
              name: "leftHeading",
              type: "string"
            },
            {
              title: "Left Text",
              name: "leftText",
              type: "text"
            },
            {
              title: "Left Link Title",
              name: "leftLinkTitle",
              type: "string"
            },
            {
              title: "Left Url",
              name: "leftUrl",
              type: "internalLink"
            },
            {
              title: "Right Heading",
              name: "rightHeading",
              type: "string"
            },
            {
              title: "Right Text",
              name: "rightText",
              type: "text"
            },
            {
              title: "Right Link Title",
              name: "rightLinkTitle",
              type: "string"
            },
            {
              title: "Right Url",
              name: "rightUrl",
              type: "internalLink"
            },
          ]
        },
        // {
        //   name: 'leftContent',
        //   title: 'Left Content',
        //   fieldset: 'fourthPanel',
        //   type: 'array',
        //   of: [
        //     {
        //       type: 'textSection'
        //     },
        //   ],
        // },
        // {
        //   name: 'rightContent',
        //   title: 'Right Content',
        //   fieldset: 'fourthPanel',
        //   type: 'array',
        //   of: [
        //     {
        //       type: 'textSection'
        //     },
        //   ],
        // },
        // {
        //   name: 'fifthLeftHeading',
        //   type: 'string',
        //   fieldset: 'fifthPanel',
        // },
        // {
        //   name: 'fifthLeftContent',
        //   type: 'simplePortableText',
        //   fieldset: 'fifthPanel',
        // },
        // {
        //   title: "Right Content",
        //   name: "fifthRightContent",
        //   type: "simplePortableText",
        //   fieldset: "fifthPanel",
        // },
      ]
    },
    {
      title: "Background Option",
      name: "backgroundOption",
      type: "string",
      initialValue: '1',
      description: 'Choose background option',
      options: {
      list: [
        { title: 'BG 1', value: '1' },
        { title: 'BG 2', value: '2' },
      ],
      layout: 'radio', // <-- defaults to 'dropdown'
      },
    },
    {
      name: 'seo',
      type: 'seo',
      title: 'SEO',
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'M/Construction',
      }
    }
  }
}