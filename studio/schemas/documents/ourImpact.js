import Tabs from "sanity-plugin-tabs"
export default {
  name: 'ourImpact',
  title: 'Our Impact',
  type: 'document',
  fields: [
    {
      name: "content",
      type: "object",
      inputComponent: Tabs,
      fieldsets: [
        { name: "hero", title: "Hero", options: { sortOrder: 10 } },
        { name: "secondPanel", title: "Second Panel", options: { sortOrder: 20 } },
        { name: "thirdPanel", title: "Third Panel", options: { sortOrder: 30 } },
        { name: "fourthPanel", title: "Fourth Panel", options: { sortOrder: 40 } },
        { name: "fifthPanel", title: "Fifth Panel", options: { sortOrder: 50 } },
      ],
      options: {
        // setting layout to object will group the tab content in an object fieldset border.
        // ... Useful for when your tab is in between other fields inside a document.
        layout: "object"
      },
      fields: [
        {
          name: 'hero',
          type: 'hero',
          fieldset: 'hero',
        },
        {
          title: "Left Content",
          name: "secondLeftContent",
          type: "simplePortableText",
          fieldset: "secondPanel",
        },
        {
          title: "Block Quote",
          name: "blockQuote",
          type: "textSection",
          fieldset: "secondPanel",
        },
        {
          name: 'secondLeftBody',
          title: 'Left Body',
          fieldset: 'secondPanel',
          type: 'array',
          of: [
            {
              type: 'textSection'
            },
          ],
        },
        {
          name: 'secondRightBody',
          title: 'Right Body',
          fieldset: 'secondPanel',
          type: 'array',
          of: [
            {
              type: 'textSection'
            },
          ],
        },
        {
          title: "Testimonial",
          name: "testimonial",
          type: "reference",
          fieldset: "thirdPanel",
          to: [
            {
              type: "testimonial"
            }
          ]
        },
        {
          name: 'testimonialImage',
          type: 'image',
          fieldset: "thirdPanel",
          fields: [
            {
              name: 'alt',
              type: 'string',
              title: 'Alternative text',
            },
          ],
        },
      ]
    },
    {
      title: "Background Option",
      name: "backgroundOption",
      type: "string",
      initialValue: '1',
      description: 'Choose background option',
      options: {
      list: [
        { title: 'BG 1', value: '1' },
        { title: 'BG 2', value: '2' },
      ],
      layout: 'radio', // <-- defaults to 'dropdown'
      },
    },
    {
      name: 'seo',
      type: 'seo',
      title: 'SEO',
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'Our Impact',
      }
    }
  }
}