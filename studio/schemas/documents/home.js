import Tabs from "sanity-plugin-tabs"

export default {
  name: "home",
  title: "Home",
  type: "document",
  __experimental_actions: [/*'create',*/ 'update', /*'delete',*/ 'publish'],
  fields: [
    {
      name: "content",
      type: "object",
      inputComponent: Tabs,
      fieldsets: [
        { name: "hero", title: "Hero", options: { sortOrder: 10 } },
        { name: "secondPanel", title: "Second Panel", options: { sortOrder: 20 } },
        { name: "thirdPanel", title: "Third Panel", options: { sortOrder: 30 } },
        { name: "popup", title: "Popup", options: { sortOrder: 40 } },
      ],
      options: {
        // setting layout to object will group the tab content in an object fieldset border.
        // ... Useful for when your tab is in between other fields inside a document.
        layout: "object"
      },

      fields: [
        {
          type: "object",
          name: "hero",
          title: "Hero Section",
          fieldset: "hero",
          fields: [
            {
              title: "Title",
              name: "title",
              type: "string"
            },
            {
              title: "Caption",
              name: "caption",
              type: "text"
            },
            {
              title: "Gallery",
              name: "gallery",
              type: "gallery",
            },
          ]
        },
        {
          title: "Second Panels Section",
          name: "secondPanelsSection",
          type: "object",
          fieldset: "secondPanel",
          fields: [
            {
              title: "Left Text",
              name: "leftText",
              type: "text"
            },
            {
              title: "Left Link Title",
              name: "leftLinkTitle",
              type: "string"
            },
            {
              title: "Left Link",
              name: "leftLink",
              type: "internalLink"
            },
            {
              title: "Right Text",
              name: "rightText",
              type: "text"
            },
            {
              title: "Right Link Title",
              name: "rightLinkTitle",
              type: "string"
            },
            {
              title: "Right Link",
              name: "rightLink",
              type: "internalLink"
            },
          ]
        },
        {
          title: "Third Panel Section",
          name: "thirdPanelSection",
          fieldset: "thirdPanel",
          type: "object",
          fields: [
            {
              title: "Left Heading",
              name: "leftHeading",
              type: "string"
            },
            {
              title: "Left Text",
              name: "leftText",
              type: "text"
            },
            {
              title: "Left Link Title",
              name: "leftLinkTitle",
              type: "string"
            },
            {
              title: "Left Url",
              name: "leftUrl",
              type: "internalLink"
            },
            {
              title: "Right Heading",
              name: "rightHeading",
              type: "string"
            },
            {
              title: "Right Text",
              name: "rightText",
              type: "text"
            },
            {
              title: "Right Link Title",
              name: "rightLinkTitle",
              type: "string"
            },
            {
              title: "Right Url",
              name: "rightUrl",
              type: "internalLink"
            },
          ]
        },  
        {
          title: "Popup",
          name: "popupSection",
          fieldset: "popup",
          type: "object",
          fields: [
            {
              title: "Show Popup",
              name: "isPopup",
              type: "boolean"
            },
            {
              title: "Title",
              name: "title",
              type: "string"
            },
            {
              title: "Content",
              name: "content",
              type: "text"
            },
            {
              title: "Button Text",
              name: "buttonText",
              type: "string"
            },
          ]
        }  
      ]
    },
  ],
  preview: {
    prepare() {
      return {
        title: `Home Page`,
      };
    },
  }
}