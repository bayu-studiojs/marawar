import { MdLink } from 'react-icons/md'

export default {
  name: 'route',
  type: 'document',
  title: 'Route',
  icon: MdLink,
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      options: {
        source: 'title',
        maxLength: 96,
      },
      validation: (Rule) => Rule.required(),
    },
    // {
    //   name: 'page',
    //   type: 'reference',
    //   description: 'Select the page that this route should point to',
    //   to: [
    //     {
    //       type: 'page',
    //     },
    //   ],
    // },
    // {
    //   name: 'includeInSitemap',
    //   type: 'boolean',
    //   title: 'Include page in sitemap',
    //   description: 'For search engines. Will be added to /sitemap.xml',
    // },
    // {
    //   name: 'disallowRobots',
    //   type: 'boolean',
    //   title: 'Disallow in robots.txt',
    //   description: 'Hide this route for search engines',
    // },
  ],
  preview: {
    select: {
      slug: 'slug.current',
      pageTitle: 'title',
    },
    prepare({ slug, pageTitle }) {
      return {
        subtitle: slug === '/' ? '/' : `/${slug}`,
        title: `${pageTitle}`,
      }
    },
  },
}
