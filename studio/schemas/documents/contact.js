
export default {
  name: 'contact',
  title: 'Contact',
  type: 'document',
  __experimental_actions: ['create', 'update', /*'delete',*/ 'publish'],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'caption',
      title: 'Caption',
      type: 'text',
      rows: 5,
    },  
    {
      name: 'seo',
      title: 'Seo',
      type: 'seo',
    },
  ],
  preview: {
    prepare() {
      return {
        title: `Contact`,
      };
    },
  },
}
