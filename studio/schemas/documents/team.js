export default {
  name: 'team',
  title: 'Team',
  type: 'document',
  // __experimental_actions: ['create', 'update', 'publish', 'delete'],
  fields: [
    {
      name: 'personsName',
      title: 'Persons Name',
      type: 'string',
      initialValue: '',
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'position',
      title: 'Position',
      type: 'string',
      initialValue: '',
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'image',
      title: 'Person Image',
      description: 'Upload JPEG image - 122px x 1200px @72dpi',
      type: 'image',
      validation: (Rule) => Rule.required(),
      options: {
        hotspot: true,
      },
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
          initialValue: ''
        },
      ],
    },
    {
      name: 'body',
      title: 'Body',
      type: 'simplePortableText',
    },
    {
      name: 'quote',
      title: 'Quote',
      type: 'simplePortableText',
    },
  ],
  preview: {
    select: {
      title: 'personsName',
      subtitle: 'position',
      media: 'image',
    },
  },
}
