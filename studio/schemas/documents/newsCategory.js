import { IoPricetagOutline } from "react-icons/io5";

export default {
  title: "News Categories",
  name: "newsCategory",
  type: "document",
  icon: IoPricetagOutline,
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string"
    },
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      options: {
        source: 'title'
      }
    },
    {
      title: "Description",
      name: "description",
      type: "text",
      rows: 3
    }
  ],
  preview: {
    select: {
      title: 'title',
    },
  },
}
