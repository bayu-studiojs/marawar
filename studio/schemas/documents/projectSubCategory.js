import { IoPricetagsOutline } from "react-icons/io5";

export default {
  title: "Project Sub Category",
  name: "projectSubCategory",
  type: "document",
  icon: IoPricetagsOutline,
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string"
    },
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      options: {
        source: 'title'
      }
    },
    {
      title: "Description",
      name: "description",
      type: "text",
      rows: 3
    },
    {
      title: "Parent Category",
      name: "parentCategory",
      type: "reference",
      to: [{type: 'projectCategory'}]
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'parentCategory.title',
    },
    prepare({ title, subtitle }) {
      console.log(subtitle, 'sub')
      return {
        title: `${title}`,
        subtitle: `${subtitle}`,
        media: '☆'
      };
    },
  },
}