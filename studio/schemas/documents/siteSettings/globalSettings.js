export default {
  name: 'globalSettings',
  type: 'document',
  title: 'Global Settings',
  __experimental_actions: ['create', 'update', /*'delete',*/ 'publish'],
  fieldsets: [
    { name: 'contact', title: 'Contact Details' },
    { name: 'address1', title: 'Address 1' },
    { name: 'address2', title: 'Address 2' },
  ],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Site title',
    },
    {
      title: 'URL',
      name: 'url',
      type: 'url',
      description: 'The main site url. Used to create canonical url',
    },
    {
      title: 'Brand logo',
      description:
        'Best choice is to use an SVG where the color are set with currentColor',
      name: 'logo',
      type: 'image',
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
          description: 'Important for SEO and accessiblity.',
          options: {
            isHighlighted: true,
          },
        },
      ],
    },
    {
      name: 'phone',
      title: 'Phone',
      type: 'object',
      fieldset: 'contact',
      fields: [
        {
          name: 'office',
          title: 'Office',
          type: 'string',
          validation: Rule => Rule.regex(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/).error('eg: +(123) - 456-78-90'),
        },
        {
          name: 'emergency',
          title: 'After Hours Emergency Response Team:',
          type: 'string',
          validation: Rule => Rule.regex(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/).error('eg: +(123) - 456-78-90'),
        },
      ]
    },
    {
      name: 'email',
      title: 'Email Address',
      type: 'email',
      fieldset: 'contact',
      validation: (Rule) =>
      Rule.regex(
          /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
          {
            name: 'email', // Error message is "Does not match email-pattern"
            invert: false, // Boolean to allow any value that does NOT match pattern
          }
        ),
      },
    {
      name: 'address',
      title: 'Address',
      type: 'array',
      fieldset: 'contact',
      of: [
        {
          name: 'addressItem',
          title: 'Address Item',
          type: 'object',
          fields: [
            {
              name: 'name',
              title: 'Name',
              type: 'string',
              
            },
            {
              name: 'location',
              title: 'Location',
              type: 'string',
            },
            {
              title: 'Map',
              name: 'geopoint',
              type: 'geopoint',
            },     
          ],
        },
      ]
    },
    {
      name: 'seo',
      type: 'seo',
      title: 'SEO',
    },
    {
      name: 'cta',
      type: 'cta',
    },
    {
      name: 'social',
      type: 'social',
      title: 'Social media links',
    },
  ],
  preview: {
    prepare: () => {
      return {
        title: 'Global Settings',
      }
    },
  },
}
