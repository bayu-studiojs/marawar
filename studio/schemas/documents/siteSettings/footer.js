export default {
  name: 'footer',
  type: 'document',
  title: 'Footer',
  // https://www.sanity.io/docs/experimental/ui-affordances-for-actions
  __experimental_actions: ['create', 'update', /*'delete',*/ 'publish'],
  fieldsets: [
    { name: 'footer', title: 'Footer' },
    { name: 'footerTop', title: 'Footer Top' },
    { name: 'footerMiddle', title: 'Footer Middle' },
    { name: 'footerBottom', title: 'Footer Bottom' },
  ],
  fields: [
    {
      name: 'footerTopLogo',
      title: 'Logo',
      type: 'image',
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
          description: 'Important for SEO and accessiblity.',
          options: {
            isHighlighted: true,
          },
        },
      ],
      fieldset: 'footerTop'
    },
    {
      name: 'footerTopText',
      title: 'Text',
      type: 'text',
      rows: 4,
      fieldset: 'footerTop'
    },
    {
      name: 'footerTopLinkTitle',
      title: 'Link Title',
      type: 'string',
      fieldset: 'footerTop'
    },
    {
      name: 'footerTopLink',
      title: 'Link',
      type: 'internalLink',
      fieldset: 'footerTop'
    },
    {
      name: 'col1',
      title: 'Column 1',
      type: 'titleAndText',
      fieldset: 'footerMiddle',
      options: {
        collapsible: true,
      }
    },
    {
      name: 'col2',
      title: 'Column 2',
      type: 'object',
      fields: [
        {
          name: 'title',
          title: 'Title',
          type: 'string',
        },
        {
          title: 'Menu',
          name: 'col2Menu',
          description: 'Select pages for the column 2 menu',
          validation: (Rule) => [
            Rule.max(6).warning('Are you sure you want more than 5 items?'),
            Rule.unique().error('You have duplicate menu items'),
          ],
          type: 'array',
          of: [
            {
              type: 'internalLink'
            },
          ],
        },
      ],
      fieldset: 'footerMiddle',
      options: {
        collapsible: true,
      }
    },
    {
      name: 'col3',
      title: 'Column 3',
      type: 'object',
      fields: [
        {
          name: 'title',
          title: 'Title',
          type: 'string',
        },
        {
          title: 'Menu',
          name: 'col3Menu',
          description: 'Select pages for the column 2 menu',
          validation: (Rule) => [
            Rule.max(6).warning('Are you sure you want more than 5 items?'),
            Rule.unique().error('You have duplicate menu items'),
          ],
          type: 'array',
          of: [
            {
              type: 'reference',
              to: [{ type: 'route' }],
            },
          ],
        },
      ],
      fieldset: 'footerMiddle',
      options: {
        collapsible: true,
      }
    },
    {
      name: 'col4',
      title: 'Column 4',
      type: 'object',
      fields: [
        {
          name: 'title',
          title: 'Title',
          type: 'string',
        },
        {
          title: 'Address 1',
          name: 'address1Footer',
          type: 'string',
          options: {
            source: 'address1Name'
          }
        },
        {
          title: 'Address 2',
          name: 'address2Footer',
          type: 'string',
          options: {
            source: 'address2Name'
          }
        },
        {
          name: 'socialMedia',
          title: 'Social Media',
          type: 'array',
          of: [
            {
              name: 'item',
              title: 'Item',
              type: 'object',
              fields: [
                { 
                  name: 'platform',
                  type: 'string',
                  options: {
                    list: [
                      {title: 'Facebook', value: 'facebook'},
                      {title: 'Twitter', value: 'twitter'},
                      {title: 'LinkedIn', value: 'linkedin'},
                      {title: 'Instagram', value: 'instagram'},
                      {title: 'Youtube', value: 'youtube'},
                    ], // <-- predefined values
                  },
                },
                {
                  name: 'url',
                  type: 'url',
                }
              ],
            },
          ],
        },
      ],
      fieldset: 'footerMiddle',
      options: {
        collapsible: true,
      }
    },
    {
      name: 'copright',
      title: 'Copyright',
      type: 'string',
      fieldset: 'footerBottom'
    },
    {
      name: 'link1Title',
      title: 'Link 1 Title',
      type: 'string',
      fieldset: 'footerBottom'
    },
    {
      name: 'link1Url',
      title: 'Link 1',
      type: 'url',
      fieldset: 'footerBottom'
    },
    {
      name: 'link2Title',
      title: 'Link 2 Title',
      type: 'string',
      fieldset: 'footerBottom'
    },
    {
      name: 'link2Url',
      title: 'Link 2',
      type: 'internalLink',
      fieldset: 'footerBottom'
    },
    {
      name: 'showSearch',
      title: 'Show Search?',
      type: 'boolean',
      initialValue: true,
      fieldset: 'footerBottom'
    },
  ],
  preview: {
    prepare: () => {
      return {
        title: 'Footer',
      }
    },
  },
}
