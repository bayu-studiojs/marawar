export default {
  name: 'mainNavigation',
  type: 'document',
  title: 'Main Navigation',
  __experimental_actions: ['create', 'update', /*'delete',*/ 'publish'],
  fields: [
    {
      title: 'Menu Items',
      name: 'children',
      type: 'array',
      of: [{ type: 'menuItem' }, { type: 'menuBranch' }, {type: 'menuCustom'}],
      validation: Rule => Rule.required(),
    },
  ],
  preview: {
    prepare: () => {
      return {
        title: 'Main Navigation',
      }
    },
  },
}
