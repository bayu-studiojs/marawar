export default {
  name: 'news',
  title: 'News',
  type: 'document',
  __experimental_actions: ['create','update', 'delete','publish'],
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      options: {
        source: 'title',
        maxLength: 96,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Categories",
      name: "categories",
      type: "array",
      of: [
          {
            name: 'name',
            title: 'name',
            type: 'reference',
            to: [{type: 'newsCategory'}]
          },
      ],
    },
    {
      title: "Publish Date",
      name: "publishDate",
      type: "date",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "MainImage",
      name: "mainImage",
      type: "image",
      options: {
        hotspot: true,
      },
      description:
        'Ideally, this image is re-sized to 1600px wide x 1066px high @72dpi and saved as a JPEG before uploading.',
        validation: (Rule) => Rule.required(),
    },
    {
      title: "Body",
      name: "body",
      type: 'blockContent',
    },
    {
      title: "Featured",
      name: "featured",
      type: "boolean",
      initialValue: false,
    },
    {
      title: 'Excerpt',
      name: 'excerpt',
      type: 'text',
      rows: 3,
      description: 'Displays as intro text when shown as featured.'
    },
    {
      name: 'seo',
      type: 'seo',
      title: 'SEO',
    },
  ],
  orderings: [
    {
      title: 'Published Date',
      name: 'publishedDate',
      by: [
        {field: 'publishDate', direction: 'asc'}
      ]
    },
    {
      title: 'A-Z',
      name: 'title',
      by: [
        {field: 'title', direction: 'asc'}
      ]
    },
    
  ],
  preview: {
    select: {
      title: 'title',
      media: 'mainImage',
      featured: 'featured',
      publishDate: 'publishDate',
    },
    prepare({ title, media, featured, publishDate }) {
      return {
        title: `${title}`,
        subtitle: `${publishDate} ${featured ? '| Featured' : ''}`,
        media,
      };
    },
  },
}
