// import bcp47 from 'bcp47'
// import IconPicker from '../objects/iconPicker'

export default {
  name: 'service',
  type: 'document',
  title: 'Services',
  // https://www.sanity.io/docs/experimental/ui-affordances-for-actions
  // __experimental_actions: ['create', 'update', 'publish', 'delete'],
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      options: {
        source: 'title',
        maxLength: 96,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Excerpt",
      name: "excerpt",
      type: "text",
      rows: 5,
    },
    {
      title: "Main Image",
      name: "mainImage",
      type: "image",
      options: {
        hotspot: true,
      },
      description:
        'Ideally, this image is re-sized to 1600px wide x 1066px high @72dpi and saved as a JPEG before uploading.',
        validation: (Rule) => Rule.required(),
    },
    {
      title: "Body",
      name: "body",
      type: 'array',
      of: [{type: 'block'}]
    },
    {
      title: "Service Bullets",
      name: "serviceBullets",
      type: 'array',
      of: [
        {
          type: 'block', 
          lists: [
            {title: 'Numbered', value: 'number'},
            {title: 'Bullet', value: 'bullet'},
          ],
          styles: [],
          marks: {}
        }
      ]
    },
    {
      title: "Testimonial",
      name: "testimonial",
      type: "reference",
      to: [
        {
          type: "testimonial"
        }
      ]
    },
    {
      name: 'testimonialImage',
      type: 'image',
      fields: [
        {
          name: 'alt',
          type: 'string',
          title: 'Alternative text',
        },
      ],
    },
    {
      name: 'summary',
      type: 'text',
      rows: 5,
    },
    {
      title: "CTA",
      description: 'Optional',
      name: 'cta',
      type: "cta"
    },
    {
      title: "SEO",
      name: 'seo',
      type: "seo"
    }
  ],
  preview: {
    select: {
      title: 'title',
      media: 'mainImage',
    }
  },
}
