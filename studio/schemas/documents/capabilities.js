import Tabs from "sanity-plugin-tabs"

export default {
  name: "capability",
  title: "Capabilities",
  type: "document",
  __experimental_actions: [/*'create',*/ 'update', /*'delete',*/ 'publish'],
  fields: [
    {
      name: "content",
      type: "object",
      inputComponent: Tabs,
      fieldsets: [
        { name: "hero", title: "Hero", options: { sortOrder: 10 } },
        { name: "secondPanel", title: "Second Panel", options: { sortOrder: 20 } },
        { name: "thirdPanel", title: "Service & Testimonial", options: { sortOrder: 30 } },
        { name: "safetySection", title: "Safety", options: { sortOrder: 40 } },
        { name: "partners", title: "Our Partners in Business", options: { sortOrder: 40 } },
      ],
      options: {
        // setting layout to object will group the tab content in an object fieldset border.
        // ... Useful for when your tab is in between other fields inside a document.
        layout: "object"
      },
      fields: [
        {
          type: "object",
          name: "heroGallery",
          title: "Hero Section",
          fieldset: "hero",
          fields: [
            {
              title: "Title",
              name: "title",
              type: "string"
            },
            {
              title: "Caption",
              name: "caption",
              type: "text"
            },
            {
              title: "Gallery",
              name: "gallery",
              type: "gallery",
            },
          ]
        },
        {
          title: "Body",
          name: "body",
          type: "textSection",
          fieldset: "secondPanel",
        },
        {
          title: "Block Quote",
          name: "blockQuote",
          type: "textSection",
          fieldset: "secondPanel",
        },
        {
          title: "Services",
          name: "service",
          type: "array",
          fieldset: "thirdPanel",
          of: [
            {
              name: 'service',
              type: "reference",
              to: [
                {
                  type: "service"
                }
              ]
            }
          ]
        },
        {
          title: "Testimonial",
          name: "testimonial",
          type: "reference",
          fieldset: "thirdPanel",
          to: [
            {
              type: "testimonial"
            }
          ]
        },
        {
          name: 'testimonialImage',
          type: 'image',
          fieldset: "thirdPanel",
          fields: [
            {
              name: 'alt',
              type: 'string',
              title: 'Alternative text',
            },
          ],
        },
        {
          title: "Heading",
          name: "heading",
          type: "string",
          fieldset: "safetySection",
        },
        {
          title: "Body Left Column",
          name: "bodyLeftColumn",
          type: "object",
          fieldset: "safetySection",
          fields: [
            {
              name: "content",
              type: "simplePortableText"
            },
            {
              name: 'fileUpload',
              type: 'fileUpload'
            },
            {
              name: 'specifications',
              type: 'array',
              of: [
                {
                  name: 'item',
                  title: 'Item',
                  type: 'object',
                  fields: [
                    {
                      name: 'title',
                      type: 'string',
                      validation: Rule => Rule.required()
                    },
                    {
                      name: 'code',
                      type: 'string',
                      validation: Rule => Rule.required()
                    }
                  ],
                },
              ]
            }
          ]
        },
        {
          title: "Body Right Column",
          name: "bodyRightColumn",
          type: "simplePortableText",
          fieldset: "safetySection",
        },
        {
          title: "Logo Gallery",
          name: "logoGallery",
          type: "gallery",
          fieldset: "safetySection",
        },
        {
          title: "Partner",
          name: "partner",
          type: "array",
          fieldset: 'partners',
          of: [
            {
              type: 'urlAndImage'
            }
          ]
        },
        {
          title: "Text Col Left",
          name: "textColLeft",
          type: "simplePortableText",
          fieldset: 'partners'
        },
        {
          title: "Text Col Right",
          name: "textColRight",
          type: "simplePortableText",
          fieldset: 'partners'
        },
      ]
    },
    {
      title: "Background Option",
      name: "backgroundOption",
      type: "string",
      initialValue: '1',
      description: 'Choose background option',
      options: {
      list: [
        { title: 'BG 1', value: '1' },
        { title: 'BG 2', value: '2' },
      ],
      layout: 'radio', // <-- defaults to 'dropdown'
      },
    },
    {
      name: 'seo',
      type: 'seo',
      title: 'SEO',
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'Capabilities',
      }
    }
  }
}