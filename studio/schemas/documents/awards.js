export default {
  name: 'awards',
  title: 'Awards',
  type: 'document',
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Publish Date",
      name: "publishDate",
      type: "datetime",
      initialValue: (new Date()).toISOString()
    },
    {
      title: "Type",
      name: "type",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Provider",
      name: "provider",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "MainImage",
      name: "mainImage",
      type: "image",
      options: {
        hotspot: true,
      },
      description:
        'Ideally, this image is re-sized to 1600px wide x 1066px high @72dpi and saved as a JPEG before uploading.',
        validation: (Rule) => Rule.required(),
    },
    
  ],
  preview: {
    select: {
      title: 'title',
      media: 'mainImage',
    },
  },
}
