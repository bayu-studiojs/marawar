
export default {
  name: 'privacyPolicy',
  title: 'Privacy Policy',
  type: 'document',
  __experimental_actions: ['create', 'update', /*'delete',*/ 'publish'],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'content',
      title: 'Content',
      type: 'simplePortableText',
    },  
    {
      name: 'seo',
      title: 'Seo',
      type: 'seo',
    },
  ],
}
