import Tabs from "sanity-plugin-tabs"

export default {
  name: "ourPeople",
  title: "Our People",
  type: "document",
  __experimental_actions: [/*'create',*/ 'update', /*'delete',*/ 'publish'],
  fields: [
    {
      name: "content",
      type: "object",
      inputComponent: Tabs,
      fieldsets: [
        { name: "hero", title: "Hero", options: { sortOrder: 10 } },
        { name: "secondPanel", title: "Second Panel", options: { sortOrder: 20 } },
        { name: "thirdPanel", title: "Management Team", options: { sortOrder: 30 } },
        { name: "fourthPanel", title: "Partnership", options: { sortOrder: 40 } },
      ],
      options: {
        // setting layout to object will group the tab content in an object fieldset border.
        // ... Useful for when your tab is in between other fields inside a document.
        layout: "object"
      },
      fields: [
        {
          title: "Hero",
          name: "hero",
          type: "hero",
          fieldset: 'hero',
        },
        // second panel
        {
          name: 'leftColumn',
          title: 'Left Column',
          fieldset: 'secondPanel',
          type: 'array',
          of: [
            {
              name: 'leftContent',
              type: 'textSection',
            },
          ],
        },
        {
          name: 'rightColumn',
          title: 'Right Column',
          fieldset: 'secondPanel',
          type: 'array',
          of: [
            {
              name: 'rightContent',
              type: 'textSection',
            },
          ],
        },
        {
          name: 'teamOrder',
          title: 'Team Order',
          fieldset: 'thirdPanel',
          type: 'array',
          of: [
            {
              name: 'person',
              type: 'reference',
              to: { type: 'team'}
            },
          ],
        },
        {
          title: "Partner",
          name: "partner",
          type: "array",
          fieldset: 'fourthPanel',
          of: [
            {
              type: 'urlAndImage'
            }
          ]
        },
        {
          title: "Text Col Left",
          name: "textColLeft",
          type: "simplePortableText",
          fieldset: 'fourthPanel'
        },
        {
          title: "Text Col Right",
          name: "textColRight",
          type: "simplePortableText",
          fieldset: 'fourthPanel'
        },
      ]
    },
    {
      title: "Background Option",
      name: "backgroundOption",
      type: "string",
      description: 'Choose background option',
      options: {
        list: [
          { title: 'BG 1', value: '1' },
          { title: 'BG 2', value: '2' },
        ],
        layout: 'radio', // <-- defaults to 'dropdown'
      },
    },
    {
      name: 'seo',
      type: 'seo',
      title: 'SEO',
    },
  ],
  preview: {
    
    prepare() {
      return {
        title: `Our People`,
      };
    },
  },
}