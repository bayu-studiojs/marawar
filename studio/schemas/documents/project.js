export default {
  name: "project",
  title: "Projects",
  type: "document",
  fields: [
    {
      title: "Title",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      options: {
        source: 'title',
        maxLength: 96,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Categories",
      name: "categories",
      type: "array",
      of: [
          {
            name: 'name',
            title: 'name',
            type: 'reference',
            to: [{type: 'projectCategory'}]
          },
      ],
      validation: (Rule) => Rule.unique().min(1),
    },
    // {
    //   title: "Sub Category",
    //   name: "subcategory",
    //   type: "array",
    //   options: {
    //     sortable: false,
    //   },
    //   of: [
    //     {
    //       type: 'reference',
    //       to: [
    //         { title: 'Sub Category', type: 'projectSubCategory' }
    //       ],
    //       options: {
    //         filter: ({ document }) => {
    //           const parent = document.category && document.category._ref;
    //           // "false" to disallow adding subcategories without a parent
    //           console.log(document.category, 'parent')
    //           const filter = parent ? "parentCategory._ref == $parent" : "false";
    //           return {
    //             filter,
    //             params: {
    //               parent
    //             }
    //           }
    //         }
    //       },
    //     },
    //   ],
    // },
    {
      title: "Publish Date",
      name: "publishDate",
      type: "date",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Featured",
      name: "featured",
      type: "boolean",
      initialValue: false,
    },
    {
      title: "Service Type",
      name: "serviceType",
      type: 'reference',
      to: [{type: 'service'}],
      hidden: true,
    },
    {
      title: "Main Image",
      name: "mainImage",
      description: "Ideally, this image is re-sized to 1600px wide x 1066px high @72dpi and saved as a JPEG before uploading.",
      type: "image",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Client",
      name: "client",
      type: "string",
      initialValue: "",
    },
    {
      title: "Timeframe",
      name: "timeframe",
      type: "string",
      initialValue: "",
    },
    {
      title: "Value",
      name: "value",
      type: "string",
      initialValue: "",
      hidden: true,
    },
    {
      title: "Location",
      name: "location",
      type: "string",
      initialValue: "",
    },
    {
      title: "Body",
      name: "body",
      type: "blockContent"
    },
    {
      title: "Breakout Quotes",
      name: "breakoutQuotes",
      type: "blockContent"
    },
    // {
    //   title: "Testimonial",
    //   name: "testimonial",
    //   type: "reference",
    //   to: [
    //     {
    //       type: "testimonial"
    //     }
    //   ],
    //   initialValue: "",
    // },
    {
      title: "Gallery",
      name: "gallery",
      type: "gallery",
    },
    {
      title: "Seo",
      name: "seo",
      type: "seo"
    }
  ],
  preview: {
    select: {
      title: 'title',
      media: 'mainImage',
      featured: 'featured',
      publishDate: 'publishDate',
    },
    prepare({ title, media, featured, publishDate }) {
      return {
        title: `${title}`,
        subtitle: `${publishDate} ${featured ? '| Featured' : ''}`,
        media,
      };
    },
  },
}