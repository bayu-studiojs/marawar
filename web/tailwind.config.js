/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  // mode: 'jit',
  content: [
    'components/**/*.vue',
    'layouts/**/*.vue',
    'pages/**/*.vue',
    'plugins/**/*.js',
    'nuxt.config.js',
  ],
  theme: {
    fontFamily: {
      sans: '"Adelle Sans", -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
    },
    extend: {
      textColor: {
        primary: '#2C2D2D',
      },
      colors: {
        'm-blue': '#385DAE',
        'm-darkgray': '#2C2D2D',
        'm-beige-light': '#F0EEEA',
        'm-beige': '#D8C9B1',
        'm-beige-mid': '#eae1d3',
        white: '#ffffff',
        'm-border': '#DBDBDB',
      },
      lineHeight: {
        big: '0.8em',
      },
      fontSize: {
        'm-base': '15px',
      },
      typography: {
        DEFAULT: {
          css: {
            figure: {
              figcaption: {
                color: 'grey',
                marginTop: '1rem',
                fontSize: '1rem',
              },
            },
            p: {
              fontSize: '15px',
              marginBottom: '1rem',
              marginTop: '1rem',
              lineHeight: 'normal',
            },
            strong: {
              color: 'inherit',
            },
            ul: {
              listStyleType: 'disc',
              listStylePosition: 'inside',
              li: {
                fontSize: '15px',
                margin: 0,
                lineHeight: 'normal',
                paddingLeft: 0,
              },
            },
            h3: {
              fontFamily: '"National", sans-serif',
              fontSize: '2.25rem',
              marginTop: 0,
              marginBottom: '1rem',
              lineHeight: 'normal',
            },
          },
        },
      },
      borderWidth: {
        DEFAULT: '1px',
        0: '0',
        1: '1px',
        2: '2px',
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
  ],
  corePlugins: {
    margin: true,
    container: true,
  },
  // purge: {
  //   // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
  //   enabled: process.env.NODE_ENV === 'production',
  //   content: [
  //     'components/**/*.vue',
  //     'layouts/**/*.vue',
  //     'pages/**/*.vue',
  //     'plugins/**/*.js',
  //     'nuxt.config.js',
  //   ],
  // },
}
