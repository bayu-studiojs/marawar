/* eslint-disable new-cap */
import AOS from 'aos'

// trigger when the Nuxt page is ready
window.onNuxtReady(() => {
  window.scrollTo({ top: 0, behavior: 'auto' })
  AOS.refresh()
})

export default ({ app }) => {
  app.AOS = new AOS.init({
    disable() {
      const maxWidth = 800
      return window.innerWidth < maxWidth
    },
    once: true,
    startEvent: 'load',
  }) // eslint-disable-line new-cap
}
