import { groq } from '@nuxtjs/sanity'

const query = groq`
{
  'mainNavigation': *[_type == "mainNavigation"][0]{
    title,
    "slug": slug.current,
    "children": children[]{
      _type == "menuCustom" => {
        _key,
        _type,
        "name": title,
        "slug": slug.current,
      },
      _type == "menuItem" => {
        _key,
        _type,
        "name": project -> title,
        "slug": slug.current,
        "slug": project -> slug.current,
      },
      _type == "menuBranch" => {
        ...,
        _key,
        _type,
        name,
        "slug": slug.current,
        "children": children[] {
          ...,
          _key,
          _type == "menuCustom" => {
            "name": title,
            "slug": slug.current
          },
          _type == "menuItem" => {
            "name": project -> title,
            "slug": project -> slug.current,
          }
        }
      }
    }
  },
	'globalSettings': *[_type == 'globalSettings'][0]
	{
    ...,
    "cta": cta{
      ...,
      route->{"slug": slug.current, title}
	  }
  },
  'footer': *[_type == 'footer'][0]
	{
    ...,
    "col2": col2{
    	title,
      "menu": col2Menu[]->{"slug": slug.current, title}
    },
    "col3": col3{
    	title,
      "menu": col3Menu[]->{"slug": slug.current, title}
    },
    footerTopLink->{"slug": slug.current, title},
    link2Url->{"slug": slug.current}
  },
}
`

/**
 * We do this to achieve server side rendering for
 * content displayed by layouts components
 * ( layouts does not have an asyncData() method )
 */
export default ({ store, $sanity }) => {
  return $sanity.fetch(query).then((data) => {
    store.commit('globalData', data)
  })
}
