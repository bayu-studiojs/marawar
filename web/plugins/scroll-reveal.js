import Vue from 'vue'
import VueScrollReveal from 'vue-scroll-reveal'

// Using ScrollReveal's default configuration
Vue.use(VueScrollReveal, {
  // class: '', // A CSS class applied to elements with the v-scroll-reveal directive; useful for animation overrides.
  origin: 'bottom',
  opacity: 0,
  duration: 1400,
  delay: 250,
  scale: 1,
  distance: '60px',
  easing: 'ease-in-out',
  mobile: false,
  viewOffset: {
    top: 150,
  },
})
