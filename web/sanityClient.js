import sanityClient from '@sanity/client'

export default sanityClient({
  // Find your project ID and dataset in `sanity.json` in your studio project
  projectId: 'e94jy5ip',
  dataset: 'production',
  useCdn: false,
  apiVersion: 'v2021-06-07',
  // token: process.env.SANITY_API_TOKEN,
  withCredentials: true,
  // use a UTC date string
  // useCdn == true gives fast, cheap responses using a globally distributed cache.
  // Set this to false if your application require the freshest possible
  // data always (potentially slightly slower and a bit more expensive).
})
