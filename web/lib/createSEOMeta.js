export const createSEOMeta = (data) => {
  const metaStucture = [
    {
      hid: 'description',
      name: 'description',
      content: data.description || '',
    },
    {
      hid: 'og:description',
      property: 'og:description',
      content: data.description || '',
    },
    {
      property: 'og:title',
      vmid: 'og:title',
      hid: 'og:title',
      content: data.title,
    },
    {
      property: 'title',
      vmid: 'title',
      hid: 'title',
      content: data.title,
    },
    {
      hid: 'og:image',
      property: 'og:image',
      content: data.image,
    },
    {
      hid: 'og:url',
      property: 'og:url',
      content: data.url,
    },
    {
      hid: 'twitter:card',
      name: 'twitter:card',
      content: data.cardType || 'summary_large_image',
    },
    {
      hid: 'twitter:image:alt',
      name: 'twitter:image:alt',
      content: data.shareDesc || 'summary_large_image',
    },
    {
      hid: 'og:site_name',
      name: 'og:site_name',
      property: 'og:site_name',
      content: 'Marawar',
    },
  ]
  return metaStucture
}
