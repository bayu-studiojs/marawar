// import sanityClient from './sanityClient'
// // import createSeoMeta from './plugins/createSeoMeta'

// const routesQuery = `
//   {
//     "news": *[_type == "news" && defined(slug.current)],
//     "ourProject": *[_type == "project" && defined(slug.current)] {slug,"category": coalesce(categories[0]->{"slug": slug.current}, "")},
//     "capabilities": *[_type == "service" && defined(slug.current)],
//   }
//   `

// const routeGenerator = function () {
//   const routes = []

//   sanityClient.fetch(routesQuery).then((res) => {
//     res.news.map((item) => routes.push({ url: `/news/${item.slug.current}` }))
//     res.ourProject.map((item) =>
//       routes.push({
//         url: `/our-projects/${item.category.slug}`,
//       })
//     )
//     res.ourProject.map((item) =>
//       routes.push({
//         url: `/our-projects/${item.category.slug}/${item.slug.current}`,
//       })
//     )
//     res.capabilities.map((item) =>
//       routes.push({ url: `/capabilities/${item.slug.current}` })
//     )
//   })

//   return routes
// }
// const routes = routeGenerator()

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  // ssr: process.env.NODE_ENV === 'production',
  ssr: true,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // loading
  loading: {
    color: '#395DAE',
    height: '5px',
    throttle: 0,
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Marawar',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // ...createSeoMeta({
      //   title: '',
      // }),
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src: 'https://kit.fontawesome.com/58a1e4ff09.js',
        async: true,
        crossorigin: 'anonymous',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/tailwind.css',
    // '~/assets/scss/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vue-rellax', mode: 'client' },
    // { src: '~/plugins/aos.client', mode: 'client' },
    { src: '~/plugins/vue-lazyload', mode: 'client' },
    { src: '~/plugins/scroll-reveal', mode: 'client' },
    // { src: '~/plugins/gmap' },
    { src: '~/plugins/scrollto' },
    '~/plugins/globalData',
    '~/plugins/sanity-image-builder',
    '~/plugins/createSeoMeta',
    '~/plugins/checkObjectKey',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: ['~/components', '~/components/home', '~/components/serializers'],
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    // '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/style-resources',
    'nuxt-font-loader',
    '@nuxtjs/dotenv',
    ['@nuxtjs/sanity/module', { apiVersion: 'v2021-06-07' }],
    '~/plugins/sitemapGenerator',
    '@nuxtjs/google-analytics',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-validate',
    'vue-scrollto/nuxt',
    [
      'nuxt-gmaps',
      {
        key: 'AIzaSyBXEtm3k95k9GJ_b_bvBf5QRqIs-geI0Uo',
        // you can use libraries: ['places']
      },
    ],
    '@nuxtjs/sitemap',
    [
      '@nuxtjs/robots',
      {
        Sitemap: 'https://www.marawar.com.au/sitemap.xml',
      },
    ],
    ['cookie-universal-nuxt'],
    '@nuxtjs/axios',
    'nuxt-rfg-icon',
    '@nuxtjs/manifest',
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // extractCSS: true,
    postcss: {
      postcssOptions: {
        plugins: {
          'postcss-nested': {},
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },

  // fontLoader: {
  //   url: {
  //     local: '/css/fonts.css',
  //   },
  //   prefetch: false,
  //   preconnect: false,
  //   preload: {
  //     local: {
  //       hid: 'National-preload',
  //       importance: 'high',
  //     },
  //   },
  // },
  router: {
    linkActiveClass: 'menu-active',
    linkExactActiveClass: 'menu-exact-active',
    middleware: ['redirectMiddleware'],
  },
  // serverMiddleware: ['./server/middleware/redirectMiddleware'],
  generate: {
    fallback: true,
    // routes: () => {
    //   return sanityClient.fetch(routesQuery).then((res) => {
    //     return [
    //       ...res.news.map((item) => `/news/${item.slug.current}`),
    //       ...res.ourProject.map(
    //         (item) => `/our-projects/${item.category.slug}`
    //       ),
    //       ...res.ourProject.map(
    //         (item) => `/our-projects/${item.category.slug}/${item.slug.current}`
    //       ),
    //       ...res.capabilities.map(
    //         (item) => `/capabilities/${item.slug.current}`
    //       ),
    //     ]
    //   })
    // },
    // routes: routes.map(route => route.url)
  },
  // sitemap: {
  //   hostname: 'https://www.marawar.com.au',
  //   // routes, // all the dynamic routes
  // },
  // sitemap() {
  //   const { route } = require('./plugins/sitemapGenerator')
  //   console.log(route.routeGenerator, 'routeGenerator config')
  //   return {
  //     hostname: 'https://www.marawar.com.au',
  //     routes: route.routeGenerator,
  //   }
  // },
  sitemap: {
    hostname: 'https://www.marawar.com.au',
    gzip: true,
    // exclude: [
    //   '/exclude-one',
    //   '/exclude-two'
    // ],
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date(),
    },
  },
  googleAnalytics: {
    id: process.env.GOOGLE_ANALYTICS_ID,
  },
  publicRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID,
    },
  },
}
