const redirects = [
  {
    from: '/civil-industrial-and-infrastructure',
    to: '/capabilities/civil-industrial-and-infrastructure',
  },
  {
    from: '/education',
    to: '/capabilities/education',
  },
  {
    from: '/fit-outs-and-refurbishments',
    to: '/capabilities/fit-outs-and-refurbishments',
  },
  {
    from: '/health-services',
    to: '/capabilities/health-services',
  },
  {
    from: '/maintenance-and-minor-works',
    to: '/capabilities/maintenance-and-minor-works',
  },
  {
    from: '/residential-and-commercial',
    to: '/capabilities/residential-and-commercial',
  },
  // { from: "^/units/$", to: "/units" }
]

export default (req, res, next) => {
  const redirect = redirects.find((r) => r.from === req.url)

  if (redirect) {
    res.writeHead(301, { Location: redirect.to })
    res.end()
  } else {
    next()
  }

  // if (req.url === '/civil-industrial-and-infrastructure') {
  //   res.redirect(301, '/capabilities/civil-industrial-and-infrastructure/')
  //   return
  // }
  // next()
}
