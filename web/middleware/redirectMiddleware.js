// const redirects = [
//   {
//     from: '/civil-industrial-and-infrastructure',
//     to: '/capabilities/civil-industrial-and-infrastructure',
//   },
//   {
//     from: '/education',
//     to: '/capabilities/education',
//   },
//   {
//     from: '/fit-outs-and-refurbishments',
//     to: '/capabilities/fit-outs-and-refurbishments',
//   },
//   {
//     from: '/health-services',
//     to: '/capabilities/health-services',
//   },
//   {
//     from: '/maintenance-and-minor-works',
//     to: '/capabilities/maintenance-and-minor-works',
//   },
//   {
//     from: '/residential-and-commercial',
//     to: '/capabilities/residential-and-commercial',
//   },
//   {
//     from: '/capabilities/contact-us',
//     to: '/contact-us',
//   },
//   {
//     from: '/our-projects/residential-and-commercial/contact-us',
//     to: '/contact-us',
//   },
//   {
//     from: '/our-projects/maintenance-and-minor-works/contact-us',
//     to: '/contact-us',
//   },
//   {
//     from: '/our-projects/fit-outs-and-refurbishments/contact-us',
//     to: '/contact-us',
//   },
//   {
//     from: '/our-projects/health-services/contact-us',
//     to: '/contact-us',
//   },
//   {
//     from: '/our-projects/civil-industrial-and-infrastructure/contact-us',
//     to: '/contact-us',
//   },
//   {
//     from: '/service/armadale-health-service-copy',
//     to: 'news/new-contract-armadale-health-service',
//   },
//   // { from: "^/units/$", to: "/units" }
// ]

export default function (ctx) {
  // if (ctx.route.fullPath === '/about-us') ctx.redirect(301, '/about')
  // const redirect = redirects.find((r) => r.from === ctx.route.fullPath)
  // if (redirect) {
  //   // ctx.redirect(301, redirect.to)
  //   ctx.redirect(301, '/maintenance')
  // }
  if (ctx.route.fullPath === '/') {
    // ctx.redirect(301, redirect.to)
    ctx.redirect(301, '/maintenance')
  } else if (ctx.route.fullPath !== '/maintenance') {
    ctx.redirect(301, '/maintenance')
  }
}
