// import Vuex from 'vuex'

// const createStore = () => {
//   return new Vuex.Store({
//     mutations: {
//       globalData(state, value) {
//         state.globalData = value
//       },
//       routeList(state, value) {
//         state.routeList = value
//       },
//     },
//   })
// }

// export default createStore

export const state = () => ({
  loading: true,
  loadingCategory: false,
  error: null,
  globalData: {},
})

export const mutations = {
  setLoading(state, value) {
    state.loading = value
  },
  setLoadingCategory(state, value) {
    state.loadingCategory = value
  },
  globalData(state, value) {
    state.globalData = value
  },
  setError(state, value) {
    state.error = value
  },
  routeList(state, value) {
    state.routeList = value
  },
}
